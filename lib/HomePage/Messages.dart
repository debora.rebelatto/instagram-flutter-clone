import 'package:flutter/material.dart';

class Messages extends StatefulWidget{
  _MessagesState createState() => _MessagesState();
}

class _MessagesState extends State<Messages> {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Messages")),
      body: Container(height: 50, color: Colors.blue),
      bottomNavigationBar: Container(height: 50, color: Colors.grey[850]),
    );
  }
}