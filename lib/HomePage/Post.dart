import 'package:flutter/material.dart';

class Post extends StatefulWidget{
  _PostState createState() => _PostState();
}

class _PostState extends State<Post> {
  bool liked = false;
  Widget build(BuildContext context) {
    return SingleChildScrollView(child:Container(
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(children: [
            Padding(padding: EdgeInsets.all(10),
              child: Container(
                height: 50,
                width: 50,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  color: Colors.blue
                ),
              )
            ),
            Text('Nome'),
          ],),
          Container( height: 300, color: Colors.white, ),
          Row(children: [
            IconButton(
              icon: Icon(
                liked == true ? Icons.favorite : Icons.favorite_border,
                color: liked == true ? Colors.red : null,
              ),
              onPressed: () { setState(() { liked = !liked; }); },
            ),
          ],)
        ]
      ))
    );
  }
}