import 'package:flutter/material.dart';

import 'Messages.dart';
import 'Post.dart';
import 'Stories.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.black,
      appBar: AppBar(
        // backgroundColor: Colors.grey[850],
        title: Text('Digi'),
        actions: [
          IconButton(
            icon: Icon(Icons.near_me_outlined, color: Colors.white),
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(
                builder: (context) => Messages()
              ));
            },
          )
        ],
      ),

      body: Container(
        child: SingleChildScrollView(
          child: Column(children: [
            Stories(),
            Post(),
            Post(),
          ])
        ,)
      ),
      bottomNavigationBar: BottomNavigationBar(
       currentIndex: _currentIndex, // this will be set when a new tab is tapped
       items: [
         BottomNavigationBarItem(
           icon: new Icon(Icons.home),
           title: new Text('Home'),
         ),
         BottomNavigationBarItem(
           icon: new Icon(Icons.mail),
           title: new Text('Messages'),
         ),
         BottomNavigationBarItem(
           icon: Icon(Icons.person),
           title: Text('Profile')
         )
       ],
     ),

    );
  }
int _currentIndex = 0;
  void onTabTapped(int index) {
   setState(() {
     _currentIndex = index;
   });
 }
}
