import 'package:flutter/material.dart';

class Stories extends StatefulWidget{
  _StoriesState createState() => _StoriesState();
}

class _StoriesState extends State<Stories> {
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        height: 120,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: 20,
          shrinkWrap: true,
          itemBuilder: (context, index) {
            return Padding(
              padding: EdgeInsets.all(10),
              child: Container(
              width: 100,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50), color: Colors.blue
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [ Container(height: 20,child: Text("$index"))]
              )
            )
            );


          }
        )
      )
    );
  }
}